#include "includes.h"
#include "utils.h"
MODULE_LICENSE("GPL");

extern struct mutex kbschar_mutex;
extern struct nf_hook_ops nfhook;
extern int              majorNumber;
extern struct class*    kbscharClass;
extern struct device*   kbscharDevice;
extern char message[256];
extern short size_of_message;
extern char   state[128];


int device_open(struct inode *inodepointer, struct file *filepointer) {
    if (!mutex_trylock(&kbschar_mutex)) {
        printk(KERN_ALERT "KBS: Could not open. Mutex lock in place\n");
        return -EBUSY; // Why do we return this?
    }
    printk(KERN_INFO "KBS: Opened!\n");
    return 0;
}


ssize_t  dev_read(struct file *filepointer, char *buffer, size_t length_of_buffer, loff_t *offset) {
    printk(KERN_INFO "Called dev_read\n");
    // copy_to_user < figure out what this does...
    // It's format is (* to, * from, size)
    if (copy_to_user(buffer, message, size_of_message) == 0) { // Successfully copeid!
        printk(KERN_INFO "KBS: Sent %d bytes to the user\n", size_of_message);
        return (size_of_message = 0); // Clear the message and return
    }
    else {
        printk(KERN_ALERT "KBS: Couldn't copy message to user!\n");
        return 2; // 2 for error
    }

    return 0;
    
}


ssize_t  dev_write(struct file *filepointer, const char *buffer, size_t length_of_buffer, loff_t *offset) {
    sprintf(message, "%s", buffer);
    size_of_message = strlen(message);
    
    if ( strcmp(message, "block" ) == 0 ) {
        nf_register_net_hook(&init_net, &nfhook);
        printk(KERN_INFO "nfilter: %s\n", state);
        strcpy(state, "blocked");
    }
    else if ( strcmp(message, "accept") == 0 ) {
        nf_unregister_net_hook(&init_net, &nfhook);
        printk(KERN_INFO "nfilter: %s\n", state);
        strcpy(state, "unblocked");
    }
    else
        printk(KERN_INFO "Didn't recieve recognized instruction. State still at %s", state);
    

    printk(KERN_INFO "KBS: Recieved: %s\n", message);
    return size_of_message;
}


int dev_release(struct inode *inodepointer, struct file *filepointer) {
    mutex_unlock(&kbschar_mutex);
    printk(KERN_INFO "KBS: Released\n");
    return 0;
}

unsigned int process_packet(void *priv, struct sk_buff *skb, const struct nf_hook_state *state) {
    
    return NF_DROP;
}
