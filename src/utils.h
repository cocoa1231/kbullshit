#include "includes.h"
#ifndef H_UDEF_PROTOS
#define H_UDEF_PROTOS

int      device_open(struct inode *, struct file *); // Had to call this device because of 
                                                            // duplicate definition in netfilter.h
ssize_t  dev_read(struct file *, char *, size_t, loff_t *);
int      dev_release(struct inode *, struct file *);
ssize_t  dev_write(struct file *, const char *, size_t, loff_t *);
unsigned int process_packet(void *, struct sk_buff *, const struct nf_hook_state *);

#endif
