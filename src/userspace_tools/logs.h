#ifndef __LOGS_H
#define __LOGS_H 

#include <iostream>

struct logs {
    char time_of_action[26];     // The _ is a quick and dirty way to fix the naming issue
    char action[7];             // block or unblock
    char status[1];            // Write fail or not
};

class logger {
private:
    logs *log_entry;
public:
    ssize_t log(char *, char *, ssize_t);
};

#endif /* ifndef __LOGS_H */
