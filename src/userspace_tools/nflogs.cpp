#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <string>
#include <string.h>
#include <algorithm>
#include <fstream>
#include "logs.h"
#define MAX_ACTION_LEN 7

std::string formatted_time() {
    time_t t = time(0);
    struct tm *curtime = localtime( &t );
    return asctime(curtime);   
}

int main(int argc, char *argv[])
{
    std::ifstream logfile ("/var/log/nfilter.log", std::ios::in | std::ios::binary);
    struct logs membuff;
    std::cout << "Time\t\t\t\tAction\tStatus" << std::endl;
    while ( !logfile.eof() ) {
        logfile.read((char*)&membuff, sizeof(membuff));
        std::string time(membuff.time_of_action);
        std::string action(membuff.action);
        std::string status(membuff.status);
    
        time.erase(std::remove(time.begin(), time.end(), '\n'), time.end());

        std::cout << time << "\t"
                  << action <<  "\t"
                  << status << std::endl;
    }
    return 0;
}
