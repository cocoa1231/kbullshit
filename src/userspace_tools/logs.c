#include <iostream>
#include <string.h>
#include <fstream>
#include "logs.h"

ssize_t logger::log(char *time, char *action, ssize_t result) {
    strcpy(time, logger::log_entry->time_of_action);
    strcpy(action, logger::log_entry->action);
    logger::log_entry->status = result;

    std::fstream logfile ("/var/log/nfilter.log", std::ios::binary | std::ios::app);

    return 0;
}
