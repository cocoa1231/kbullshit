#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <string>
#include <string.h>
#include <algorithm>
#include <fstream>
#include "logs.h"

std::string formatted_time() {
    time_t t = time(0);
    struct tm *curtime = localtime( &t );
    return asctime(curtime);   
}

void log(std::string action, int status) {

    std::fstream logfile;
    std::string  ftime = formatted_time();
    ftime.erase(std::remove(ftime.begin(), ftime.end(), '\n'), ftime.end());

    logfile.open("/var/log/nfilter.log", std::ios::binary | std::ios::app);
    
    struct logs *data = new struct logs;
    std::sprintf(data->time_of_action, "%s", ftime.c_str());
    std::sprintf(data->action, "%s", action.c_str());
    std::sprintf(data->status, "%d", status);

    
    logfile.write((char*)data, sizeof(logs));

}

int main(int argc, char *argv[]) {
    int fd;
    if ( !(argc > 1) ) {
        std::cout << "nfilter: Stands for no filter. The most secure firewall out there. Blocks literally everything." << std::endl;
        std::cout << "Can't get attacked if you don't accept any packets whatsoever. My logic is infallible\n" << std::endl;

        std::cout << "Options:" << std::endl;
        std::cout << "\t--accept" << std::endl;
        std::cout << "\t\tAccept all packets" << std::endl;
        std::cout << "\t--block" << std::endl;
        std::cout << "\t\tBlock  all packets\n\n" << std::endl;
    }
    for (int i = 0; i < argc; ++i) {
        std::string arg = argv[i];
        if ( arg == "--accept" ) {
            fd = open("/dev/kbschar", O_RDWR);
            std::string data = "accept";
            ssize_t write_status = write(fd, data.c_str(), data.size());
            log(data, write_status);

            if ( write_status == -1 ) {
                std::cout << "Error: Could not write to /dev/kbschar" << std::endl;
                return -1;
            }
            std::cout << "Accepting packets now" << std::endl;
            return 0;
        }
        if ( arg == "--block" ) {
            fd = open("/dev/kbschar", O_RDWR);
            std::string data = "block";
            ssize_t write_status = write(fd, data.c_str(), data.size());

            log(data, write_status);

            if ( write_status == -1 ) {
                std::cout << "Error: Could not write to /dev/kbschar" << std::endl;
                return -1;
            }
            std::cout << "Blocking packets now" << std::endl;
            return 0;
        }
        
    }
    return 0;
}
