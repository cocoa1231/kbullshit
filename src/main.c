#include "utils.h"
#include "includes.h"
#define DEVICE_NAME "kbschar"
#define CLASS_NAME  "kbs"


MODULE_LICENSE("GPL");
MODULE_AUTHOR("cocoa");
MODULE_DESCRIPTION("Some kernel bullshit I wrote");
MODULE_VERSION("0.1");

static int __init kbs_entry(void);
static void __exit out(void);

int              majorNumber;
char             state[128];
struct class*    kbscharClass = NULL;
struct device*   kbscharDevice = NULL;
char message[256] = {0};
short size_of_message = 0;
DEFINE_MUTEX(kbschar_mutex);

struct nf_hook_ops nfhook = {
    .hook = process_packet,
    .hooknum = NF_INET_PRE_ROUTING,
    .pf = PF_INET,
    .priority = NF_IP_PRI_FIRST
};

EXPORT_SYMBOL( nfhook );
EXPORT_SYMBOL( state );


struct file_operations fops =
{
    .open = device_open,
    .read = dev_read,
    .write = dev_write,
    .release = dev_release,
};

static int __init kbs_entry(void) {
    
    /*
     * Two things. Register the major number, and then create a chardev with
     * said major number
     */

    printk(KERN_INFO "Kernel Bullshit: Initializing kbs\n");
    
    // Register Major Number
    majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
    if (majorNumber < 0) {
        printk(KERN_ALERT "KBS: Failed to register chardev\n");
        return majorNumber;
    }
    printk(KERN_INFO "KBS: Registered chardev successfully with major number %d\n", majorNumber);

    // Register Class
    kbscharClass = class_create(THIS_MODULE, CLASS_NAME);
    if (IS_ERR(kbscharClass)) {
        unregister_chrdev(majorNumber, DEVICE_NAME);
        printk(KERN_ALERT "KBS: Failed to register device class\n");
        return PTR_ERR(kbscharClass);
    }
    printk(KERN_INFO "KBS: Device class registered correctly\n");


    // Register device driver
    kbscharDevice = device_create(kbscharClass, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
    if (IS_ERR(kbscharDevice)) {
        class_destroy(kbscharClass);
        unregister_chrdev(majorNumber, DEVICE_NAME);
        printk(KERN_ALERT "KBS: Failed to create the device\n");
        pr_err("Error Code: %ld\n", PTR_ERR(kbscharDevice));
        return PTR_ERR(kbscharDevice);
    }
    printk(KERN_INFO "KBS: Device class created successfully!\n");
    
    mutex_init(&kbschar_mutex);
    return 0;
}

static void __exit out(void) {
    mutex_destroy(&kbschar_mutex);
    device_destroy(kbscharClass, MKDEV(majorNumber, 0));
    class_unregister(kbscharClass);
    class_destroy(kbscharClass);
    unregister_chrdev(majorNumber, DEVICE_NAME);
    printk(KERN_INFO "Kernel Bullshit: Exiting\n");
}

module_init(kbs_entry);
module_exit(out);
